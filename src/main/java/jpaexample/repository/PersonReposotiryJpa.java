package jpaexample.repository;

import jpaexample.domain.Person;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@SuppressWarnings("JpaQlInspection")
@Transactional
@Repository
public class PersonReposotiryJpa implements PersonRepository{

    @PersistenceContext
    private EntityManager em;

    @Override
    public Person getById(int id){
        return em.find(Person.class, id);
    }

    @Override
    public void insert(Person pers){
        em.persist(pers);
    }

    @Override
    public Person getFirst(){
        TypedQuery<Person> query = em.createQuery("select p from Person p where p.id = 1", Person.class);
        return query.getSingleResult();
    }

    @Override
    public List<Person> getAllPersons(){
        TypedQuery<Person> query = em.createQuery("select p from Person p", Person.class);
        return query.getResultList();
    }

    @Override
    public Person getbyName(String name){
        TypedQuery<Person> query = em.createQuery("select p from Person p where p.name = :name", Person.class);
        query.setParameter("name", name);
        return query.getSingleResult();
    }

}


