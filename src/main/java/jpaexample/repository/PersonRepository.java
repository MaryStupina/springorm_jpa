package jpaexample.repository;

import jpaexample.domain.Person;

import java.util.List;

public interface PersonRepository {
    Person getById(int id);

    void insert(Person pers);

    Person getFirst();

    List<Person> getAllPersons();

    Person getbyName(String name);
}
