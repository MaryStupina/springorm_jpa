package jpaexample;

import jpaexample.domain.Person;
import jpaexample.repository.PersonRepository;
import org.h2.tools.Console;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.sql.SQLException;
import java.util.List;

@SpringBootApplication
public class JpaExampleApplication {

    public static void main(String[] args) throws SQLException {
        ApplicationContext context = SpringApplication.run(JpaExampleApplication.class, args);
        PersonRepository personRepository = context.getBean(PersonRepository.class);

        personRepository.insert(new Person("Ivan"));
        Person nullPerson1 = personRepository.getById(1);
//        System.out.println(nullPerson1.getName());
        Person person2 = personRepository.getFirst();
//        System.out.println(person2.getName());
        List<Person> persons = personRepository.getAllPersons();
//        for (Person p : persons) {
//            System.out.println(p.getName());
//        }

        Person namePerson = personRepository.getbyName("Ivan");
//        System.out.println(namePerson.getName());

        Console.main(args);
    }

}
